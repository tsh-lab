/* 
* tsh - A tiny shell program with job control
* 
* <Put your name and login ID here>
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

/* Misc manifest constants */
#define MAXLINE    1024   /* max line size */
#define MAXARGS     128   /* max args on a command line */
#define MAXJOBS      16   /* max jobs at any point in time */
#define MAXJID    1<<16   /* max job ID */

/* Job states */
#define UNDEF 0 /* undefined */
#define FG 1    /* running in foreground */
#define BG 2    /* running in background */
#define ST 3    /* stopped */

/*Parse states */
#define TNORMAL   0x0  /*argument*/
#define TINFILE   0x1   /*input file*/
#define TOUTFILE  0x2  /*output file*/

/* 
* Jobs states: FG (foreground), BG (background), ST (stopped)
* Job state transitions and enabling actions:
*     FG -> ST  : ctrl-z
*     ST -> FG  : fg command
*     ST -> BG  : bg command
*     BG -> FG  : fg command
* At most 1 job can be in the FG state.
*/

/* Global variables */
extern char **environ;      /* defined in libc */
char prompt[] = "tsh> ";    /* command line prompt (DO NOT CHANGE) */
int verbose = 0;            /* if true, print additional output */
int nextjid = 1;            /* next job ID to allocate */
char sbuf[MAXLINE];         /* for composing sprintf messages */

struct job_t {              /* The job struct */
	pid_t pid;              /* job PID */
	int jid;                /* job ID [1, 2, ...] */
	int state;              /* UNDEF, BG, FG, or ST */
	char cmdline[MAXLINE];  /* command line */
};
struct job_t job_list[MAXJOBS]; /* The job list */

struct cmdline_tokens {
	int argc;               /* Number of arguments */
	char *argv[MAXARGS];    /* The arguments list */
	char *infile;           /* The input file */
	char *outfile;          /* The output file */
	enum builtins_t         /* Indicates if argv[0] is a builtin command */
		{none, quit, jobs, bg, fg} builtins;
};

/* End global variables */


/* Function prototypes */

/* Here are the functions that you will implement */
void eval(char *cmdline);
int builtin_cmd(char **argv);
void do_bgfg(char **argv);
void waitfg(pid_t pid);

void sigchld_handler(int sig);
void sigtstp_handler(int sig);
void sigint_handler(int sig);

/* Here are helper routines that we've provided for you */
int parseline(const char *cmdline, struct cmdline_tokens *tok); 
void sigquit_handler(int sig);

void clearjob(struct job_t *job);
void initjobs(struct job_t *jobs);
int maxjid(struct job_t *jobs); 
int addjob(struct job_t *jobs, pid_t pid, int state, char *cmdline);
int deletejob(struct job_t *jobs, pid_t pid); 
pid_t fgpid(struct job_t *jobs);
struct job_t *getjobpid(struct job_t *jobs, pid_t pid);
struct job_t *getjobjid(struct job_t *jobs, int jid); 
int pid2jid(pid_t pid); 
void listjobs(struct job_t *jobs);

void usage(void);
void unix_error(char *msg);
void app_error(char *msg);
typedef void handler_t(int);
handler_t *Signal(int signum, handler_t *handler);

/*
* main - The shell's main routine 
*/
	int main(int argc, char **argv) 
{
	char c;
	char cmdline[MAXLINE];
	int emit_prompt = 1; /* emit prompt (default) */

	/* Redirect stderr to stdout (so that driver will get all output
	* on the pipe connected to stdout) */
		dup2(1, 2);

	/* Parse the command line */
	while ((c = getopt(argc, argv, "hvp")) != EOF) {
		switch (c) {
			case 'h':             /* print help message */
			usage();
			break;
			case 'v':             /* emit additional diagnostic info */
			verbose = 1;
			break;
			case 'p':             /* don't print a prompt */
			emit_prompt = 0;  /* handy for automatic testing */
			break;
			default:
			usage();
		}
	}

	/* Install the signal handlers */

	/* These are the ones you will need to implement */
	Signal(SIGINT,  sigint_handler);   /* ctrl-c */
	Signal(SIGTSTP, sigtstp_handler);  /* ctrl-z */
	Signal(SIGCHLD, sigchld_handler);  /* Terminated or stopped child */

	/* This one provides a clean way to kill the shell */
	Signal(SIGQUIT, sigquit_handler); 

	/* Initialize the job list */
	initjobs(job_list);

	/* Execute the shell's read/eval loop */
	while (1) {

	/* Read command line */
		if (emit_prompt) {
			printf("%s", prompt);
			fflush(stdout);
		}
		if ((fgets(cmdline, MAXLINE, stdin) == NULL) && ferror(stdin))
			app_error("fgets error");
		if (feof(stdin)) { /* End of file (ctrl-d) */
			fflush(stdout);
			exit(0);
		}

	/* Evaluate the command line */
		eval(cmdline);
		fflush(stdout);
		fflush(stdout);
	} 

	exit(0); /* control never reaches here */
}

/* 
* eval - Evaluate the command line that the user has just typed in
* 
* If the user has requested a built-in command (quit, jobs, bg or fg)
* then execute it immediately. Otherwise, fork a child process and
* run the job in the context of the child. If the job is running in
* the foreground, wait for it to terminate and then return.  Note:
* each child process must have a unique process group ID so that our
* background children don't receive SIGINT (SIGTSTP) from the kernel
* when we type ctrl-c (ctrl-z) at the keyboard.  
*/
void eval(char *cmdline) 
{
	int state, child_status, jid;
	struct cmdline_tokens tok;
	sigset_t sus_mask;
	int fin, fout, retval, temp;
	pid_t pid;
	struct job_t *j;
	char *pidorjid;

	/* Parse command line */
	state = parseline(cmdline, &tok); 

	if (state == -1) return;            
	if (tok.argv[0] == NULL)  return;  
	sigprocing(SIG_BLOCK);

	/* fill the set of sus_mask with all the signals and remove the signals
	SIGCHLD, SIGINT and SIGTSTP */
		sigfillset(&sus_mask);
	sigdelset(&sus_mask, SIGCHLD);
	sigdelset(&sus_mask, SIGINT);
	sigdelset(&sus_mask, SIGTSTP);

	/* checking if the quit command was given */
	if (tok.builtins == quit) {
		exit(0);	
	}
	/* checking if the command is bg or fg */
	else if (tok.builtins == bg || tok.builtins == fg){
		pidorjid = tok.argv[1]; 

		/* check if no jid or pid was provided in the arguments */
		if(pidorjid == NULL){
			unix_error("JID or PID is required\n");
		}
		/* check if the argument provided was the jid and not the pid */

		else if(pidorjid[0] == '%'){
			jid = atoi(pidorjid += sizeof(char));

			if(jid != 0 && (j = getjobjid(job_list,jid)) == NULL){
				unix_error("No such job exists in the list\n");
			}

		}

		/* get pid based on jid and check against the job list */
		else if(atoi(pidorjid) != 0){
			if((j = getjobjid(job_list,pid2jid(atoi(pidorjid)))) == NULL) {
				unix_error("No such process in the job list\n");
			}
		}

		else{
			unix_error("Please enter a pid or jid as argument\n");
		}

		return;	    
	}
	/* continue the jobs in the group */
	if(kill(-(j->pid),SIGCONT) < 0){
		return;	    
	}

	/* if it is bg then change the state of the job to BG and if the
	command was fg then change the job state to FG and wait till the
	jobs in the foreground get done in the job list */

if(tok.builtins == bg){
	printf("[%d] (%d) %s\n", j->jid, j->pid, j->cmdline);
	j->state = BG;
}
else if(tok.builtins == fg){
	j->state = FG;
	while(j->pid == fgpid(job_list)>0);
}
else{
	unix_error("BG/FG error");
}


	/* if the command on the shell was neither of the builtin command or if it
	was job then do the following */
	else{	
		/* fork a child process to do the required commands */
		if((pid = fork()) == 0){	
			/* setting proces group id for the child */
			if(setpgid(0,0) < 0)
				unix_error("Error in setpgid");

			/* i/o redirection. only works with implementation in parseline function */
			/*
			if(tok.infile != NULL){
				if ((fin = open(tok.infile, O_RDONLY)) < 0){		    
					unix_error("Open of input file failed");		    
				}		
				dup2(fin,0);

				if ((retval = close(fin)) < 0) {		    
					unix_error("close of input file failed");		    
				}		
			}

			if(tok.outfile != NULL){		
				if ((fout = open(tok.outfile, O_RDWR|O_CREAT)) < 0){		    
					unix_error("Open of output file failed");		    
				}		
				dup2(fout,1);		

				if ((retval = close(fout)) < 0) {		    
					unix_error("close of input file failed");	    
				}		
			}
			*/
			/* jobs command. Notice the this is within the child process */
			if(tok.builtins == jobs){		
				listjobs(job_list);
				exit(0);
			}	 
	/* if it is any other command that is not defined in our builtins
	list then do the following */
	else{		
		/* foreground */
		if(state ==0){	
			sigprocing(SIG_UNBLOCK);
			/* give the child control over the terminal */
			if((temp == tcsetpgrp(0,-pid))<0)
				unix_error("Cannot take control of terminal");

			/* execute the command using execvp */
			if(execvp(tok.argv[0],tok.argv)==-1){
				unix_error("Invalid Command");
			}		
		}
		/* background */
		else if(state == 1){
			sigprocing(SIG_UNBLOCK);  

			/* execute the command using execvp */
			if(execvp(tok.argv[0],tok.argv)==-1){
				unix_error("Invalid Command");
			}	       
		}
		else{	
				/* if the child was running in the foreground */
			if(state == 0){
				child_status = 0;
				addjob(job_list,pid,1,cmdline); /* add the job to the list */

				struct job_t* job = getjobpid(job_list, pid);		
				/* keep waiting for the job in the foreground to finish. Till
				then suspend all the signals not in sus_mask */
				while(job && job->state == FG) {		   		    
					sigsuspend(&sus_mask);
				}
				/* unblock the signals which were blocked earlier in the
				method */
					sigprocing(SIG_UNBLOCK);

				/* if the job is completed then delete it from the list */
				if(!job)  
					deletejob(job_list, pid);

			}
				/* if child process was running in the background */
			else if(state == 1){		
				/* add the job to the job list */
				addjob(job_list,pid,2,cmdline);
				/* unblock the signals blocked earlier in the method */
				sigprocing(SIG_UNBLOCK);		
				/* print the job id the proces id and the command */
				printf("[%d] (%d) %s\n",pid2jid(pid),pid,cmdline);	    
			}
		}
		return;

	}

/* 
* parseline - Parse the command line and build the argv array.
* 
* Characters enclosed in single quotes are treated as a single
* argument.  Return true if the user has requested a BG job, false if
* the user has requested a FG job.  
* 
* Implement I/O redirection for extra credit.
*/
int parseline(const char *cmdline, struct cmdline_tokens *tok) 
{
	static char array[MAXLINE];          /* holds local copy of command line */
	const char delims[10] = " \t\r\n";   /* argument delimiters (white-space) */
	char *buf = array;                   /* ptr that traverses command line */
	char *next;                          /* ptr to the end of the current arg */
	char *endbuf;                        /* ptr to the end of the cmdline string */
	int bg;                              /* background job? */
	int parse_state; 		
	char *delim;		 

	strcpy(buf, cmdline);
	buf[strlen(buf)-1] = ' ';  /* replace trailing '\n' with space */
	while (*buf && (*buf == ' ')) /* ignore leading spaces */
		buf++;

	tok->infile = NULL;
	tok->outfile = NULL;

	/* Build the argv list */
	tok->argc = 0;
	if (*buf == '\'') {
		buf++;
		delim = strchr(buf, '\'');
	}
	else {
		delim = strchr(buf, ' ');
	}

	while (delim) {
		tok->argv[tok->argc++] = buf;
		*delim = '\0';
		buf = delim + 1;
		while (*buf && (*buf == ' ')) /* ignore spaces */
			buf++;

		if (*buf == '\'') {
			buf++;
			delim = strchr(buf, '\'');
		}
		else {
			delim = strchr(buf, ' ');
		}
	}
	tok->argv[tok->argc] = NULL;

	if (tok->argc == 0)  /* ignore blank line */
		return 1;

	if (!strcmp(tok->argv[0], "quit")) {                 /* quit command */
		tok->builtins = quit;
		} else if (!strcmp(tok->argv[0], "jobs")) {          /* jobs command */
			tok->builtins = jobs;
			} else if (!strcmp(tok->argv[0], "bg")) {            /* bg command */
				tok->builtins = bg;
				} else if (!strcmp(tok->argv[0], "fg")) {            /* fg command */
					tok->builtins = fg;
				} else {
					tok->builtins = none;
				}

	/* should the job run in the background? */
				if ((bg = (*tok->argv[tok->argc-1] == '&')) != 0) {
					tok->argv[--tok->argc] = NULL;
				}
				return bg;
			}

/* 
* builtin_cmd - If the user has typed a built-in command then execute
*    it immediately.  
*/
int builtin_cmd(char **argv) 
{
	return 0;     /* not a builtin command */
}

/* 
* do_bgfg - Execute the builtin bg and fg commands
*/
	void do_bgfg(char **argv) 
{
	return;
}

/* 
* waitfg - Block until process pid is no longer the foreground process
*/
	void waitfg(pid_t pid)
{
	return;
}

/*****************
* Signal handlers
*****************/

/* 
* sigchld_handler - The kernel sends a SIGCHLD to the shell whenever
*     a child job terminates (becomes a zombie), or stops because it
*     received a SIGSTOP or SIGTSTP signal. The handler reaps all
*     available zombie children, but doesn't wait for any other
*     currently running children to terminate.  
*/
void sigchld_handler(int sig) 
{
	pid_t pid;
	int child_status = -1;

	/* reap any zombies and handle different statuses */
	while((pid = waitpid(-1, &child_status, WUNTRACED|WNOHANG)) > 0){	
		int jid = pid2jid(pid);
	/* handling all the stop signals */
		if(WIFSTOPPED(child_status)) {
			printf("Job [%d] (%d) stopped by signal %d\n",
				jid,pid,SIGTSTP);
			struct job_t *job = getjobpid(job_list, pid);
			job->state = ST;
			return;
		}
	/* handling all uncaught signals */
		else if(WIFSIGNALED(child_status)) {
			if(WTERMSIG(child_status) == SIGINT) {
				printf("Job [%d] (%d) terminated by signal %d\n",
					jid,pid,SIGINT);
			}

			else if(WTERMSIG(child_status) == SIGTERM) {
				printf("Job [%d] (%d) terminated by signal %d\n",
					pid2jid(pid),pid,SIGTERM);
			}

			else{
				unix_error("Uncaught signal");
			}
		}
	/* understand why we did this */
		deletejob(job_list,pid);	
		child_status = 0;

	}
	return;

}

/* 
* sigint_handler - The kernel sends a SIGINT to the shell whenver the
*    user types ctrl-c at the keyboard.  Catch it and send it along
*    to the foreground job.  
*/
void sigint_handler(int sig) 
{
	/*foreground job */
	pid_t pid = fgpid(job_list); 
	/* if there is a foreground job then kill it with the signal recieved */
	if(pid != 0)
		kill(-pid,sig);
	return;

}

/*
* sigtstp_handler - The kernel sends a SIGTSTP to the shell whenever
*     the user types ctrl-z at the keyboard. Catch it and suspend the
*     foreground job by sending it a SIGTSTP.  
*/
void sigtstp_handler(int sig) 
{
		/*foreground job */
	pid_t pid = fgpid(job_list);   
		/* if there is a foreground job then kill it with the signal recieved */
	if(pid != 0)
		kill(-pid,sig);	
	return;   
}

/*********************
* End signal handlers
*********************/

/***********************************************
* Helper routines that manipulate the job list
**********************************************/

/* clearjob - Clear the entries in a job struct */
void clearjob(struct job_t *job) {
	job->pid = 0;
	job->jid = 0;
	job->state = UNDEF;
	job->cmdline[0] = '\0';
}

/* initjobs - Initialize the job list */
void initjobs(struct job_t *jobs) {
	int i;

	for (i = 0; i < MAXJOBS; i++)
		clearjob(&jobs[i]);
}

/* maxjid - Returns largest allocated job ID */
int maxjid(struct job_t *jobs) 
{
	int i, max=0;

	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].jid > max)
		max = jobs[i].jid;
	return max;
}

/* addjob - Add a job to the job list */
int addjob(struct job_t *jobs, pid_t pid, int state, char *cmdline) 
{
	int i;

	if (pid < 1)
		return 0;

	for (i = 0; i < MAXJOBS; i++) {
		if (jobs[i].pid == 0) {
			jobs[i].pid = pid;
			jobs[i].state = state;
			jobs[i].jid = nextjid++;
			if (nextjid > MAXJOBS)
				nextjid = 1;
			strcpy(jobs[i].cmdline, cmdline);
			if(verbose){
				printf("Added job [%d] %d %s\n", jobs[i].jid, jobs[i].pid, jobs[i].cmdline);
			}
			return 1;
		}
	}
	printf("Tried to create too many jobs\n");
	return 0;
}

/* deletejob - Delete a job whose PID=pid from the job list */
int deletejob(struct job_t *jobs, pid_t pid) 
{
	int i;

	if (pid < 1)
		return 0;

	for (i = 0; i < MAXJOBS; i++) {
		if (jobs[i].pid == pid) {
			clearjob(&jobs[i]);
			nextjid = maxjid(jobs)+1;
			return 1;
		}
	}
	return 0;
}

/* fgpid - Return PID of current foreground job, 0 if no such job */
pid_t fgpid(struct job_t *jobs) {
	int i;

	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].state == FG)
		return jobs[i].pid;
	return 0;
}

/* getjobpid  - Find a job (by PID) on the job list */
struct job_t *getjobpid(struct job_t *jobs, pid_t pid) {
	int i;

	if (pid < 1)
		return NULL;
	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].pid == pid)
		return &jobs[i];
	return NULL;
}

/* getjobjid  - Find a job (by JID) on the job list */
struct job_t *getjobjid(struct job_t *jobs, int jid) 
{
	int i;

	if (jid < 1)
		return NULL;
	for (i = 0; i < MAXJOBS; i++)
		if (jobs[i].jid == jid)
		return &jobs[i];
	return NULL;
}

/* pid2jid - Map process ID to job ID */
int pid2jid(pid_t pid) 
{
	int i;

	if (pid < 1)
		return 0;
	for (i = 0; i < MAXJOBS; i++)
	if (job_list[i].pid == pid) {
		return job_list[i].jid;
	}
	return 0;
}

/* listjobs - Print the job list */
void listjobs(struct job_t *jobs) 
{
	int i;

	for (i = 0; i < MAXJOBS; i++) {
		if (jobs[i].pid != 0) {
			printf("[%d] (%d) ", jobs[i].jid, jobs[i].pid);
			switch (jobs[i].state) {
				case BG: 
				printf("Running ");
				break;
				case FG: 
				printf("Foreground ");
				break;
				case ST: 
				printf("Stopped ");
				break;
				default:
				printf("listjobs: Internal error: job[%d].state=%d ", 
					i, jobs[i].state);
			}
			printf("%s", jobs[i].cmdline);
		}
	}
}
/******************************
* end job list helper routines
******************************/


/***********************
* Other helper routines
***********************/

/*
* usage - print a help message
*/
void usage(void) 
{
	printf("Usage: shell [-hvp]\n");
	printf("   -h   print this message\n");
	printf("   -v   print additional diagnostic information\n");
	printf("   -p   do not emit a command prompt\n");
	exit(1);
}

/*
* unix_error - unix-style error routine
*/
	void unix_error(char *msg)
{
	fprintf(stdout, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

/*
* app_error - application-style error routine
*/
	void app_error(char *msg)
{
	fprintf(stdout, "%s\n", msg);
	exit(1);
}

/*
* Signal - wrapper for the sigaction function
*/
	handler_t *Signal(int signum, handler_t *handler) 
{
	struct sigaction action, old_action;

	action.sa_handler = handler;  
	sigemptyset(&action.sa_mask); /* block sigs of type being handled */
	action.sa_flags = SA_RESTART; /* restart syscalls if possible */

	if (sigaction(signum, &action, &old_action) < 0)
		unix_error("Signal error");
	return (old_action.sa_handler);
}

/*
* sigquit_handler - The driver program can gracefully terminate the
*    child shell by sending it a SIGQUIT signal.
*/
void sigquit_handler(int sig) 
{
	printf("Terminating after receipt of SIGQUIT signal\n");
	exit(1);
}

/* helper function to either block or unblock signals using sigprocmask */
void sigprocing(int var){
	sigset_t mask; 
	/* emptying the sigset mask */
	if(sigemptyset(&mask)){
		unix_error("sigemptyset() error");
	}
	/* adding SIGCHLD, SIGINT and SIGTSTP to the sigset */
	if(sigaddset(&mask, SIGCHLD)){
		unix_error("sigaddset() error");
	}

	if(sigaddset(&mask, SIGINT)){
		unix_error("sigaddset() error");
	}

	if(sigaddset(&mask, SIGTSTP)){
		unix_error("sigaddset() error");
	}
	/* block or unblock the signals in the mask depending on the argument in
	the function */
	if(sigprocmask(var, &mask, NULL)){
		unix_error("sigprocmask() error");    
	}

	return;  
}


